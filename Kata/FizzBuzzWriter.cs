﻿namespace Kata
{
    using System.IO;

    public class FizzBuzzWriter
    {
        private readonly IFizzBuzz fizzBuzz;

        public FizzBuzzWriter(IFizzBuzz fizzBuzz)
        {
            this.fizzBuzz = fizzBuzz;
        }

        public void Write(TextWriter streamWriter, int numberOfLines)
        {
            for (var i = 1; i <= numberOfLines; i++)
            {
                streamWriter.WriteLine(fizzBuzz.GetLine(i));
            }
        }
    }
}
