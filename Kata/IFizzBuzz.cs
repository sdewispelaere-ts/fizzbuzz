﻿namespace Kata
{
    public interface IFizzBuzz
    {
        string GetLine(int lineNumber);
    }
}