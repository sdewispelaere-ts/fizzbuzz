﻿namespace Kata
{
    using System;
    using System.Collections.Generic;

    public class FizzBuzz : IFizzBuzz
    {
        public string GetLine(int lineNumber)
        {
            if (lineNumber % 3 == 0)
            {
                if (lineNumber % 5 == 0) return "FizzBuzz";
                return "Fizz";
            }

            if (lineNumber % 5 == 0) return "Buzz";
            return lineNumber.ToString();
        }
    }
}
