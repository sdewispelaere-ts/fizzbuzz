﻿using System;

namespace Kata
{
    class Program
    {
        static void Main(string[] args)
        {
            var writer = new FizzBuzzWriter(new FizzBuzz());
            writer.Write(Console.Out, 100);
            Console.ReadKey();
        }
    }
}
