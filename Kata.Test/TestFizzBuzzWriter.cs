﻿namespace Kata.Test
{
    using System;
    using System.IO;

    using Moq;

    using NUnit.Framework;

    [TestFixture]
    public class TestFizzBuzzWriter
    {
        private Mock<IFizzBuzz> _fizzBuzMock;

        private FizzBuzzWriter _fizzBuzzWriter;

        [SetUp]
        public void Setup()
        {
            _fizzBuzMock = new Mock<IFizzBuzz>();
            _fizzBuzMock.Setup(x => x.GetLine(It.IsAny<int>())).Returns((int line) => "L" + line);
            _fizzBuzzWriter = new FizzBuzzWriter(_fizzBuzMock.Object);
        }

        [Test]
        public void Write()
        {
            var stringWriter = new StringWriter();
            const int NumberOfLines = 3;
            _fizzBuzzWriter.Write(stringWriter, NumberOfLines);
            _fizzBuzMock.Verify(x => x.GetLine(It.IsAny<int>()), Times.Exactly(NumberOfLines));
            var result = stringWriter.GetStringBuilder().ToString();
            Assert.AreEqual(result, "L1\r\nL2\r\nL3\r\n");
        }
    }
}
